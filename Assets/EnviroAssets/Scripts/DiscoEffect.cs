﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiscoEffect : MonoBehaviour {

	// private RectTransform _RT;\
	//[SerializeField]
	//private GameObject _DiscoBall;
	[SerializeField]
	private float _ScaleTo = 1.1f;
	private Vector3 _StartSize;
	private void Awake()
	{
		// _RT = GetComponent<RectTransform>();
		//_DiscoBall = GetComponent<GameObject>();
		_StartSize = transform.localScale;
	}

    public void OnbeatDetected()
    {
		transform.localScale *= _ScaleTo;
		StartCoroutine(DeathCoroutine());
    }

    IEnumerator DeathCoroutine()
    {
        yield return new WaitForSeconds(0.2f);
		resizeOriginal();
    }

	private void resizeOriginal(){
        transform.localScale = _StartSize;
	}
}
