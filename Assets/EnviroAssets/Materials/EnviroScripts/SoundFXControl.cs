﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFXControl : MonoBehaviour {

	[SerializeField]
	private AudioSource _WorldBuildSource;
	[SerializeField]
	private AudioClip _WorldBuildSound;

	// Use this for initialization
	void Awake () 
	{
		_WorldBuildSource.clip=_WorldBuildSound;
		
	}
	private void Start()
	{
		_WorldBuildSource.Play();
		
	}
	
}
