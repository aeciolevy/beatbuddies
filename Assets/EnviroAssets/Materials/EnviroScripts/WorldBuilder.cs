﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBuilder : MonoBehaviour {

private Vector3 _targetPosition;
private Vector3 _startPosition;
private float _duration = 2;
private float _progression = 0;



	// Use this for initialization
	void Awake () 
	{
		_targetPosition = transform.position;
		_startPosition = transform.position + new Vector3(Random.Range(-10,10),Random.Range(-10,10),Random.Range(-10,10));
		transform.position = _startPosition;


	}
	// Update is called once per frame
	void Update () 	
	{

		StartCoroutine(DelayMove());
		
	}
	IEnumerator DelayMove()
	{
		_progression = Mathf.Min(_duration, _progression + Time.deltaTime);
		if (_progression >= _duration) transform.position = _targetPosition;
		float c = _progression / _duration;
		transform.position = Vector3.Lerp (_targetPosition, _startPosition, Mathf.Sin((Mathf.Lerp(180, 270, c)) * Mathf.Deg2Rad) + 1);
		yield return null;

	}
	/*private void BuildWorld()
	{
		{
		transform.position = Vector3.Lerp (transform.position, _TargetPosition, Time.deltaTime*_Smooth);
		_MoveComplete = true;
		}
	}*/
}