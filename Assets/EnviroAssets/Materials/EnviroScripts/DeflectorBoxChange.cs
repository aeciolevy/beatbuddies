﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeflectorBoxChange : MonoBehaviour 
{
	[SerializeField]
	private Material[] _Mat;

	private Renderer rend;
	private int i;

	private void Awake()
	{
		rend=GetComponent<Renderer>();
		rend.sharedMaterials = _Mat;

		

	}

	private void Start()
	{
		InvokeRepeating("UpdateMaterial", 0f, .5f);
	}

	private void UpdateMaterial()
	{
		//rend.sharedMaterial = _Mat[(Random.value * (_Mat.Length - 1))];
		rend.sharedMaterials[4] = _Mat[i++];
		
		if (i >= _Mat.Length)
		{
			i=0;
		}
	}
}

