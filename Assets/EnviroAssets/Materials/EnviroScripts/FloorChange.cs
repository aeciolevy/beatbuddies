﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloorChange : MonoBehaviour 
{
	[SerializeField]
	private Material[] _Mat;

	private TimeController _TimeControl;
	private Renderer rend;
	private int i = 0;

	private void Awake()
	{
		_TimeControl = FindObjectOfType(typeof(TimeController)) as TimeController;
		_TimeControl.OnBeat.AddListener(UpdateMaterial);
		rend=GetComponent<Renderer>();
		rend.sharedMaterial = _Mat[i];

		

	}

	// private void Start()
	// {
	// 	InvokeRepeating("UpdateMaterial", 0f, .5f);
	// }

	public void UpdateMaterial()
	{
		//rend.sharedMaterial = _Mat[(Random.value * (_Mat.Length - 1))];
		rend.sharedMaterial = _Mat[i++];
		
		if (i >= _Mat.Length)
		{
			i=0;
		}
	}
}
