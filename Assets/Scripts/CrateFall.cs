﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrateFall : MonoBehaviour
{

	#region GlobalVariables
	private Rigidbody _Rb;


	#endregion GlobalVariables

	#region UnityFunctions

	private void Awake()
	{
		_Rb = GetComponent<Rigidbody>();
	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Edge"))
		{
			_Rb.useGravity = true;
				
		}


	}


	#endregion UnityFunctions
	
		
}
