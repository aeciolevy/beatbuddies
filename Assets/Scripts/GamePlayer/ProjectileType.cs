﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ProjectileType 
{
	public Player Player;
	public Color Color;
}
