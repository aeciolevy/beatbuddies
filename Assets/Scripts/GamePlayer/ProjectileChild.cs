﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileChild : MonoBehaviour 
{
	#region Constants
	
		private const float RIGHT = 1.0f;
		private const float LEFT = -1.0f;
		private const float NEUTRAL = 0;
		private const float UP = 1.0f;
		private const float DOWN = -1.0f;
    
	#endregion Constants  

	[SerializeField]
	private AudioClip _BulletCrossSound;

	private AudioSource _ASource;
	private bool isNotPlayed = false;
	private bool isNotStarting = false;
	private bool isAwake = true;
	private float spareTime;
	private const float _timeToReset = 0.5f;
	private Projectile _Parent;
    private Vector3[] positionsToCheck = new Vector3[4];

    #region LifeCycle
		private void Awake()
		{
			_Parent = transform.parent.GetComponent<Projectile>();
			_ASource = GetComponentInParent<AudioSource>();
			spareTime = _timeToReset;
			InitPosition();
		}

		private void Update()
		{
			DetectPlayerClose();
			ResetTime();
		}

    #endregion LifeCycle

    #region ClassMethods
		private void ResetTime()
		{
			spareTime -= Time.deltaTime;
			if (spareTime <= 0)
			{
				isNotStarting = true;
				isNotPlayed = true;
			}
		}

		private bool DetectPlayerClose()
		{
			bool isDetected = false;
			bool isPlayer = false;
			// check if there is a collider surround
			for (int i = 0; i < positionsToCheck.Length; ++i)
			{
				Collider[] playerCollider = Physics.OverlapSphere(transform.position + positionsToCheck[i], 0.4f);
				if (playerCollider.Length > 0)
				{
					isPlayer = IsPlayer(playerCollider);
				}
				isDetected = !isDetected && playerCollider.Length > 0 && isPlayer ? true : false;
				if (isDetected && isNotPlayed && isNotStarting)
				{
					isNotPlayed = false;
					spareTime = _timeToReset;
					_ASource.clip = _BulletCrossSound;
					_ASource.Play();
				}
			}
			return isDetected;
		}

		private bool IsPlayer(Collider[] colliders)
		{
			foreach (var coll in colliders)
			{
				var player = coll.GetComponent<PlayerController>();
				if (player != null)
				{
					return true;
				}
			}
			return false;
		}

		private void InitPosition()
		{
			positionsToCheck[0] = new Vector3(RIGHT, 0, NEUTRAL);
			positionsToCheck[1] = new Vector3(LEFT, 0, NEUTRAL);
			positionsToCheck[2] = new Vector3(NEUTRAL, 0, UP);
			positionsToCheck[3] = new Vector3(NEUTRAL, 0, DOWN);
		}

		private void OnTriggerEnter(Collider other)
		{
			_Parent.TriggerDetected(other);
		}

    #endregion ClassMethods
}
