﻿
using UnityEngine;

public class Teleporter : MonoBehaviour 
{
	#region GlobalVariables

		[SerializeField]
		private Transform _TeleportExit;

		private AudioSource _Audio;

	#endregion GlobalVariables

	#region UnityFunctions

		private void Awake()
		{
			_Audio = GetComponent<AudioSource>();
		}

		private void OnTriggerEnter(Collider other)
		{
			if(other.CompareTag("Player"))
			{
				_Audio.Play();
			}
			
		}

		#endregion UnityFunctions

		#region ClassFunctions

		public void Transport(GameObject obj)
		{
			Vector3 TeleportOffset = new Vector3(0, 1, 0);
			obj.transform.position = _TeleportExit.position + TeleportOffset;

			if(obj.CompareTag("Player"))
			{
				obj.GetComponent<PlayerController>()._Sprite.transform.position = _TeleportExit.position + TeleportOffset;
			}
			

			if(obj.CompareTag("Bullet"))
			{
			
				obj.GetComponentInChildren<ProjectileChild>().transform.position = obj.transform.position;
				
			}
		}

	#endregion ClassFunctions
}