﻿using UnityEngine;

public class LifeSpan : MonoBehaviour
{

    [SerializeField]
    [Range(1, 5)]
    private float _LifeTime = 3f;

    void Start()
    {
        Destroy(gameObject, _LifeTime);
    }

          

}
