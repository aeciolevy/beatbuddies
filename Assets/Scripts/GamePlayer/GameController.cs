﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GenericList<T>
{
    public void Add(T input) { }
}

public class GameController : MonoBehaviour 
{
	#region Globalvars

		[SerializeField]
		private PlayerController _PlayerPrefab;
		
		[SerializeField]
		internal List<Vector3> _PlayerStartPosition;
		
		[SerializeField]
		private List<PlayerSetup> _PlayerSetup = new List<PlayerSetup>();

		[SerializeField]
		private int _PlayerHealthStart = 3;

		[SerializeField]
		public int MAX_BULLETS = 10;

		[SerializeField]
		public int MOVEMENTS_TO_EARN_BULLET = 4;
	
		
		[Header("Colors")]
		[SerializeField]
		public Color _P1Color;
		[SerializeField]
		public Color _P2Color;

		public List<PlayerStatus> players = new List<PlayerStatus>();

		internal bool isReady = false;

		public static string Winner;

		public static float CountDown;

		public static GameController Instance;

		[Space]
		[SerializeField]
		private StartSetupEvent StartSetup;

		internal bool _IsPaused = false;

	#endregion
	
	#region LifeCycle
	private void Awake()
	{
		if (Instance != null)
		{
			Destroy(this);
			return;
		}
		Instance = this;
		for (int i = 0; i < _PlayerStartPosition.Count; i++) 
		{
			players.Add(new PlayerStatus(0, _PlayerHealthStart));
			Instantiate(_PlayerPrefab, _PlayerStartPosition[i], Quaternion.identity).Initialize(_PlayerSetup[i]);
		}
		StartCoroutine(StartSetting());
	}

	public void GameRestart()
	{
		Destroy(this);
	}

	private IEnumerator StartSetting()
	{
		yield return new WaitForSeconds(2);
		StartSetup.Invoke();
	}

	#endregion LifeCycle
	[System.Serializable]
	public class StartSetupEvent : UnityEvent {};
}

[System.Serializable]
public class PlayerSetup {
	public Player _Player;
    public PlayerInputType _PlayerInput;
	public OSSystem _OSSytem;
}