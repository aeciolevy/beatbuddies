﻿using System.Collections.Generic;
using UnityEngine;

// Script by Paul Hanberry & Aecio Levy
// This script controls how the camera moves.
// The camera is attached to an anchor that always lerps towards the average position of the 2 players
// The camera moves closer or further away depending on how far apart the characters are
public class CameraController : MonoBehaviour
{
	#region ClassVariables

		[Header("Players")]
		[SerializeField]
		[Range(1,2)]
		private int _PlayerCount = 1;
		[SerializeField]
		private GameObject _P1;
		[SerializeField]
		private GameObject _P2;

		[Header("Camera")]
		[SerializeField]
		private GameObject _Camera;
		[SerializeField]
		[Range(3,12)]
		private float _P1CameraDistance = 5f;
		[SerializeField]
		[Range(3,20)]
		private float _CameraMaxSpeed = 12f;
		[SerializeField]
		[Range(3,12)]
		private float _CameraMinDistance = 6f;
		[SerializeField]
		[Range(0.1f ,0.5f)]
		private float _CameraAccel = 0.2f;
		
		private Vector3 _cameraAnchorPosition;
		public Vector3 _cameraPosition;

		private Vector3 _cameraAnchorVelocity;
		private Vector3 _cameraVelocity;

		private Vector3 _P1Position;
		private Vector3 _P2Position;
		private bool SwitchPos;

		private Vector3	_P1Start;
		private Vector3	_P2Start;

		private PlayerController _P1Pos;
		private PlayerController _P2Pos;

	#endregion ClassVariables

	#region UnityFunctions

		void Start ()
		{
			//Reset the camera's position relative to it's anchor
			_cameraPosition.x = 0f;
			_cameraPosition.y = 0f;
			_P1Start = GameController.Instance._PlayerStartPosition[0];
			_P2Start = GameController.Instance._PlayerStartPosition[1];

		}
		
		void Update ()
		{
			_P1Position = SwitchPos ? _P1Pos.transform.position : _P1Start;
			_P2Position = SwitchPos ? _P2Pos.transform.position : _P2Start;
			CameraCalculations();
			CameraMovement();
		}

	#endregion UnityFunctions

	#region ClassFunctions
		public void StartSetup()
		{
			var players = FindObjectsOfType<PlayerController>();
			_P1Pos = players[0];
			_P2Pos = players[1];
			SwitchPos = true;
			
		}

		//Calculates where the camera needs to be moved
		private void CameraCalculations()
		{
			if(_PlayerCount == 1)
			{
				// _cameraAnchorPosition = _P1.transform.position;
				_cameraAnchorPosition = _P1Position;
				_cameraPosition.z = -_P1CameraDistance;
			}

			if(_PlayerCount == 2)
			{
				//Finds the average position of both players, and places the anchor there
				_cameraAnchorPosition = ((_P1Position + _P2Position) / 2f);
				
				_cameraPosition.z = -Mathf.Abs(Vector3.Distance(_P1Position, _P2Position));
				if(_cameraPosition.z > -_CameraMinDistance) _cameraPosition.z = -_CameraMinDistance;
			}

		}

		//Moves the camera according to the calculations
		private void CameraMovement()
		{
			//SmoothDamp is very useful
			//Smooths out the interpolation between two points
			this.gameObject.transform.position = Vector3.SmoothDamp(this.gameObject.transform.position, _cameraAnchorPosition, ref _cameraAnchorVelocity, _CameraAccel, _CameraMaxSpeed);
			_Camera.transform.localPosition = Vector3.SmoothDamp(_Camera.transform.localPosition, _cameraPosition, ref _cameraVelocity, _CameraAccel, _CameraMaxSpeed);
		}
		
	#endregion ClassFunctions
}
