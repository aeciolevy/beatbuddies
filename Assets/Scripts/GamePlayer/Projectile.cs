﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Projectile : MonoBehaviour
{
    

    #region GlobalVars
    	[SerializeField]
		private float _ProjectileSpeed = 1f;

		[Header("Art")]
		[SerializeField]
		private GameObject _ProjectileLineSprite;
		[SerializeField]
		private GameObject _ProjectileColorSprite;

		[SerializeField]
		private List<ProjectileType> _BulletSettings = new List<ProjectileType>(2);

		[Header("Particles")]
		[SerializeField]
		private ParticleSystem _Explosion;
	
		[SerializeField]
		private ParticleSystem _ReflectorParticle;

		[Header("Sound")]
		[SerializeField]
		private AudioClip _DanDamageSound;
		
		[SerializeField]
		private AudioClip _SquinkleDamageSound;

		[SerializeField]
		private GameObject _SfxDamage;

		[SerializeField]
		private GameObject _ExSource;

		[SerializeField]
		private GameObject _RefSource;


		private SpriteRenderer _SpriteRenderer;

		private AudioSource _ASource;
		private PlayerController _Owner;
		private Rigidbody _RB;
		private List <Animator> _Anim;
		private Collider _Coll;

		private float _DirectionX;
		private float _DirectionZ;

		private bool _InTeleporter = false;

    #endregion GlobalVars

    #region LifeCycle

		private void Awake()
		{
			_Coll = GetComponentInChildren<Collider>();
			_ASource = GetComponent<AudioSource>();
			_SpriteRenderer = _ProjectileColorSprite.GetComponent<SpriteRenderer>();
		}

    #endregion LifeCycle

    #region ClassFunctions

		public void Shoot(float DirX, float DirZ, PlayerController owner)
		{
			_Owner = owner;
			SetBulletColor(_Owner._Player);
			_RB = GetComponentInChildren<Rigidbody>();
			_Anim =  new List<Animator>(GetComponentsInChildren<Animator>());

			_RB.velocity = transform.forward * _ProjectileSpeed;

			_ProjectileLineSprite.transform.rotation = this.gameObject.transform.rotation;
			_ProjectileColorSprite.transform.rotation = this.gameObject.transform.rotation;

			if(DirX == 0)
			{
				_ProjectileLineSprite.transform.Rotate(45, 0, 0, Space.World);
				_ProjectileColorSprite.transform.Rotate(45, 0, 0, Space.World);
			}
			if(DirX == 1)
			{
				_ProjectileLineSprite.transform.Rotate(0, -90, -45, Space.World);
				_ProjectileColorSprite.transform.Rotate(0, -90, -45, Space.World);
			}
			if(DirX == -1)
			{
				_ProjectileLineSprite.transform.Rotate(0, 90, 45, Space.World);
				_ProjectileColorSprite.transform.Rotate(0, 90, 45, Space.World);
			}

			_DirectionX = DirX;
			_DirectionZ = DirZ;
			_Owner = owner;

			foreach(Animator Animator in _Anim)
			{
				Animator.SetFloat("DirectionX", DirX);
				Animator.SetFloat("DirectionZ", DirZ);
			}

			
		}

    #endregion UnityFunctions

    #region ClassMethods
		public void TriggerDetected(Collider other)
		{
			var otherPlayer = other.GetComponentInParent<PlayerController>();
			if(other.CompareTag("Player"))
			{ 
				if (_Owner != otherPlayer)
				{
					AudioSource exSource = Instantiate (_ExSource, transform.position, Quaternion.identity).GetComponentInParent<AudioSource>();
					exSource.Play();
					Destroy(exSource.gameObject,exSource.clip.length);
					PlayAudioDamage();		
					Instantiate (_Explosion, other.transform.position, other.transform.rotation);
					// Decrease player health
					--GameController.Instance.players[(int)otherPlayer._Player].health;
					Destroy(gameObject);
				}
			}
			
			else if(other.CompareTag("Reflector"))
			{
				AudioSource RefSource = Instantiate (_RefSource, transform.position, Quaternion.identity).GetComponentInParent<AudioSource>();
				RefSource.Play();
				Destroy(RefSource.gameObject,RefSource.clip.length);
				Instantiate (_ReflectorParticle, other.transform.position, other.transform.rotation);

				other.gameObject.GetComponent<Reflector>().ChangeDirection(gameObject);
				
				Shoot(-_DirectionX, -_DirectionZ, _Owner);

				_InTeleporter = false;
			}
			
			else if(other.CompareTag("Destructible"))
			{
				Instantiate (_Explosion, other.transform.position, other.transform.rotation);
				AudioSource exSource = Instantiate (_ExSource, transform.position, Quaternion.identity).GetComponentInParent<AudioSource>();
				exSource.Play();
				Destroy(exSource.gameObject,exSource.clip.length);

				Destroy (gameObject);
				Destroy (other.gameObject);

			}

			else if(other.CompareTag("Teleporter") && !_InTeleporter)
			{
				_InTeleporter = true;
				other.gameObject.GetComponent<Teleporter>().Transport(gameObject);

				Shoot(_DirectionX, _DirectionZ, _Owner);

				
				
			}
			
			else if(!other.CompareTag("Edge") && !other.CompareTag("Teleporter") && !other.CompareTag("Bridge") && !other.CompareTag("Checkpoint") && !other.CompareTag("Tutorial"))
			{
				Instantiate (_Explosion, other.transform.position, other.transform.rotation);
				AudioSource exSource = Instantiate (_ExSource, transform.position, Quaternion.identity).GetComponentInParent<AudioSource>();
				exSource.Play();
				Destroy(exSource.gameObject,exSource.clip.length);
				Destroy(gameObject);
			}
		}

		

		private void PlayAudioDamage() 
		{
			if (_SquinkleDamageSound != null && _DanDamageSound != null)
			{
				AudioSource aSource = Instantiate(_SfxDamage, transform.position, Quaternion.identity).GetComponent<AudioSource>();
				aSource.clip = _Owner._Player == Player.Player1 ? _SquinkleDamageSound : _DanDamageSound;
				aSource.Play();
				Destroy(aSource.gameObject, aSource.clip.length);
			}
		}

		private void SetBulletColor(Player player) 
		{
			if (player == _BulletSettings[0].Player)
			{
				_SpriteRenderer.color = _BulletSettings[0].Color;
			} 
			else if (player == _BulletSettings[1].Player) 
			{
				_SpriteRenderer.color = _BulletSettings[1].Color;
			}	
		}
	#endregion ClassMethods
}
