﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Mover : MonoBehaviour
{
    #region Constants
		private const float RIGHT = 1.0f;
		private const float LEFT = -1.0f;
		private const float NEUTRAL = 0;
		private const float UP = 1.0f;
		private const float DOWN = -1.0f;
    #endregion Constants  

    #region Events
		[Header("Events")]
		[SerializeField]
		private MoveRightEvent MoveRight;
		[SerializeField]
		private MoveLeftEvent MoveLeft;
		[SerializeField]
		private MoveUpEvent MoveUp;
		[SerializeField]
		private MoveDownEvent MoveDown;
		// [SerializeField]
		// private Transform _Player1Spawn;
		// [SerializeField]
		// private Transform _Player2Spawn;
    #endregion Events

    #region GlobalVars
		private Player _Player;
		private PlayerInputType _PlayerInput;
		private OSSystem _OSSystem;
		
		private Rigidbody _Rb;
		private Collider _Coll;

		private bool _EnableToMove = false;
		private bool _Move;
		private bool _KeyPressed;

		private bool _HorizontalInputR = false;
		private bool _HorizontalInputL = false;
		private bool _VerticalInputU = false;
		private bool _VerticalInputD = false;

		private float _MoveStep = 1f;
		private float _DirectionX;
		private float _DirectionZ;

		private bool rightMoveOld = false;
		private bool leftMoveOld = false;
		private bool upMoveOld = false;
		private bool downMoveOld = false;

		private bool InTeleporter = false;
		private bool IsFalling = false;
		private ComboMovement _ComboMovement;
		private PlayerController _PlayerController;
		private bool IsSurroundBridge;
		private AudioSource _FallingSource;
		[SerializeField]
		private AudioClip _SQFallingClip;
		[SerializeField]
		private AudioClip _DDFallingClip;

	#endregion GlobalVars

	#region UnityFunctions
		private void Awake()
		{
			_FallingSource = GetComponent<AudioSource>();
			_Rb = GetComponent<Rigidbody>();
			_Coll = GetComponent<Collider>();
			_PlayerController = GetComponent<PlayerController>();
			_PlayerController.EnableInput += isEnableInput;
		}
		private void Start()
		{
			var playerControl = GetComponent<PlayerController>();
			if (playerControl != null)
			{
				_Player = playerControl._Player;
				_PlayerInput = playerControl._PlayerInput;
				_OSSystem = playerControl._OSSytem;
			}
			_ComboMovement = _PlayerController._ComboMovement;
		}

		private void Update()
		{
			ReadInputs();
		}

		private void FixedUpdate()
		{
			if(!IsFalling)
			{
				CheckMoveInputAndApplyMove();
			}
		}

		private void OnTriggerEnter(Collider other)
		{
			if(other.CompareTag("Teleporter") && !InTeleporter)
			{
				InTeleporter = true;
				other.gameObject.GetComponent<Teleporter>().Transport(gameObject);
			}

			if(other.CompareTag("Bridge"))
			{
				_ComboMovement._IsOverBridge = true;
			}

			if(other.CompareTag("Edge"))
			{
				if(_Player == Player.Player1)
				{
				_Rb.useGravity = true;
				IsFalling = true;
				_FallingSource.clip = _DDFallingClip;
				_FallingSource.Play();
				}
				if(_Player == Player.Player2)
				{
					_Rb.useGravity = true;
					IsFalling = true;
					_FallingSource.clip = _SQFallingClip;
					_FallingSource.Play();
				}
			}

			if(other.CompareTag("Pit"))
			{
				if(_Player == Player.Player1)
				{
					_Rb.useGravity = false;
					IsFalling = false;
					_Rb.velocity = Vector3.zero;
					_Rb.transform.position = GameController.Instance._PlayerStartPosition[0];
					this.gameObject.GetComponent<PlayerController>()._Sprite.transform.position = GameController.Instance._PlayerStartPosition[0];


				}

				else if(_Player == Player.Player2)
				{
					_Rb.useGravity = false;
					IsFalling = false;
					_Rb.velocity = Vector3.zero;
					_Rb.transform.position = GameController.Instance._PlayerStartPosition[1];
					this.gameObject.GetComponent<PlayerController>()._Sprite.transform.position = GameController.Instance._PlayerStartPosition[1];

				}
			}

			if(other.CompareTag("Checkpoint"))
			{
				if(_Player == Player.Player1)
				{
					
					GameController.Instance._PlayerStartPosition[0] = transform.position;
					
				}

				else if(_Player == Player.Player2)
				{
					GameController.Instance._PlayerStartPosition[1] = transform.position;
				}
			}
		}
	#endregion UnityFunctions

	#region ClassMethods
	
		private void isEnableInput(bool onBeat)
		{
			_KeyPressed = false;
			_EnableToMove = onBeat;
		}

		private void ReadInputs()
		{
			
			if (_EnableToMove)
			{
				bool leftMove = Input.GetKeyDown(KeyCode.LeftArrow);
				bool rightMove = Input.GetKeyDown(KeyCode.RightArrow);
				bool upMove = Input.GetKeyDown(KeyCode.UpArrow);
				bool downMove = Input.GetKeyDown(KeyCode.DownArrow);
				string suffixPlayer = _Player == Player.Player1 ? "" : "2";
				string horizontalAxis = "LeftJoystickHorizontal" + suffixPlayer;
				string horizontalDPad = "LeftJoystickHorizontal" + suffixPlayer + "DPad";
				string verticalAxis = "LeftJoystickVertical" + suffixPlayer;
				string verticalDPad = "LeftJoystickVertical" + suffixPlayer + "DPad";

				if (_PlayerInput == PlayerInputType.Joystick )
				{
					if (Input.GetAxis(horizontalAxis) == 0 && Input.GetAxis(horizontalDPad) == 0)
					{	
						leftMove = false;
						rightMove = false;
					}
					if (Input.GetAxis(horizontalAxis) < 0 || Input.GetAxis(horizontalDPad) < 0)
					{
						leftMove = true;
						rightMove = false;
					}
					if (Input.GetAxis(horizontalAxis) > 0 || Input.GetAxis(horizontalDPad) > 0)
					{
						leftMove = false;
						rightMove = true;
					}
				
                	bool notHorizontalMove = !leftMove && !rightMove;	

					if (Input.GetAxis(verticalAxis) == 0 && Input.GetAxis(verticalDPad) == 0)
					{
						upMove = false;
						downMove = false;
					}

					if (notHorizontalMove)
					{
						if (Input.GetAxis(verticalAxis) < 0 || Input.GetAxis(verticalDPad) > 0)
						{
							upMove = true;
							downMove = false;
						}
						if (Input.GetAxis(verticalAxis) > 0 || Input.GetAxis(verticalDPad) < 0)
						{
							upMove = false;
							downMove = true;
						}
					}
					
				}
				
				_HorizontalInputR |= (!rightMoveOld && rightMove) && !isThereColliderSurround(RIGHT, NEUTRAL);
				_HorizontalInputL |= (!leftMoveOld && leftMove) && !isThereColliderSurround(LEFT, NEUTRAL);
				_VerticalInputU |= (!upMoveOld && upMove) && !isThereColliderSurround(NEUTRAL, UP);
				_VerticalInputD |= (!downMoveOld && downMove) && !isThereColliderSurround(NEUTRAL, DOWN);		

				if (_HorizontalInputR)
				{
					MoveRight.Invoke();
				}

				if (_HorizontalInputL)
				{
					MoveLeft.Invoke();
				}

				if (_VerticalInputD)
				{
					MoveDown.Invoke();
				}

				if (_VerticalInputU)
				{
					MoveUp.Invoke();
				}
				rightMoveOld = rightMove;
				leftMoveOld = leftMove;
				upMoveOld = upMove;
				downMoveOld = downMove;
			}
		}

		private void CheckMoveInputAndApplyMove()
		{
			if (_EnableToMove && !_KeyPressed)
			{
				_DirectionX = _HorizontalInputL ? -1.0f : _HorizontalInputR ? 1.0f : 0;
				_DirectionZ = _VerticalInputD ? -1.0f : _VerticalInputU ? 1.0f : 0;
				_KeyPressed |= _DirectionX != 0 || _DirectionZ != 0 ? true : false;
				float newX = transform.position.x + (_MoveStep * _DirectionX);
				float newZ = transform.position.z + (_MoveStep * _DirectionZ);
				Vector3 newVector = new Vector3(newX, 0, newZ);
				_Rb.transform.position = newVector;
				InTeleporter = false;
			}
			_HorizontalInputL = false;
			_HorizontalInputR = false;
			_VerticalInputD = false;
			_VerticalInputU = false;
		}
		
		private bool isThereColliderSurround(float xOffset, float zOffset)
		{
			var vec2 = new Vector3(xOffset, 0, zOffset);
			var temp =  Physics.OverlapSphere(transform.position + vec2, 0.3f);
			if (temp.Length > 0 )
			{
				if (temp[0].CompareTag("Teleporter") || temp[0].CompareTag("Edge") || temp[0].CompareTag("Bridge") || temp[0].CompareTag("Checkpoint") || temp[0].CompareTag("Tutorial"))
				{
					if (temp[0].CompareTag("Bridge"))
					{
						IsSurroundBridge = true;
					}
					return false;
				}
				else
				{
					IsSurroundBridge = false;
					return true;
				}

			}
			else 
			{
				return false;
			}
		}
	#endregion ClassMethods

	#region EventsClasses
		[System.Serializable]
		public class MoveRightEvent : UnityEngine.Events.UnityEvent {}

		[System.Serializable]
		public class MoveLeftEvent : UnityEngine.Events.UnityEvent { }

		[System.Serializable]
		public class MoveUpEvent : UnityEngine.Events.UnityEvent { }

		[System.Serializable]
		public class MoveDownEvent : UnityEngine.Events.UnityEvent { }
	#endregion EventsClasses
}
