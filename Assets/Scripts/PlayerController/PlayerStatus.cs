﻿public class PlayerStatus
{
    public int bullets;
    public int health;
    public int combo = 0;

    public PlayerStatus(int bullet, int healthArg)
    {
        bullets = bullet;
        health = healthArg;
    }
}