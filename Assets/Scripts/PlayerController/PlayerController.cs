﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(BoxCollider))]
public class PlayerController : MonoBehaviour 
{
    public enum SpriteType {
        Move,
        Shoot
    }
    #region GlobalVars
        
        [SerializeField]
        [Range(0.05f, 0.5f)]
        private float _WindowToMove = 0.5f;

        internal OSSystem _OSSytem;

        internal PlayerInputType _PlayerInput;

        internal Player _Player;

        private bool _OnBeating = false;
        internal ComboMovement _ComboMovement;
        
        [Header("Player Sprite")]
        [SerializeField]
        private GameObject _P1SpritePrefab;
        [SerializeField]
        private GameObject _P2SpritePrefab;
        [SerializeField]
        private GameObject _SpriteAnchor;

        internal GameObject _Sprite;
        
        public event Action<bool> EnableInput = delegate {};
        
        private Animator _Anim;
        private TimeController _TimeController;

        private Vector3 _CurrentVelocity;
        private float _BartSimpson; //Hi Willy
		private bool _Move;
        private bool _KeyPressed;
        private bool _Shoot;

        [SerializeField]
        private AudioClip[] _ComboSounds = new AudioClip[4];

        [SerializeField]
        private AudioClip [] _SquinkleSounds = new AudioClip[4];

        [SerializeField]
        private ParticleSystem _ComboPS;

        private AudioClip _ComboSound;

        private AudioSource _PCAudio;

        private float _FacingX = 0;
        private float _FacingZ = 0;

        private TimeController _TimeControl;

    #endregion GlobalVars

    #region UnityFunctions
        
        private void Awake()
        {
            _TimeController = FindObjectOfType(typeof(TimeController)) as TimeController;
        }

        void Start()
        {
            _PCAudio = GetComponent<AudioSource>();
            _ComboMovement = new ComboMovement(0, 0, (int)_Player);
            _ComboMovement.ResetVariables += ResetMovement;
            _ComboMovement.ComboFilled += PlayCombo;
            _TimeController.OnBeat.AddListener(OnbeatDetected);
            if(_Player == Player.Player1)
            {
                _Sprite = Instantiate(_P1SpritePrefab, this.gameObject.transform.position, _SpriteAnchor.transform.rotation);
                _Anim = _Sprite.GetComponent<Animator>();
            }
            else if(_Player == Player.Player2)
            {
                _Sprite = Instantiate(_P2SpritePrefab, this.gameObject.transform.position, _SpriteAnchor.transform.rotation);
                _Anim = _Sprite.GetComponent<Animator>();
            }
        }

        private void OnDisable()
        {
            _TimeController.OnBeat.RemoveListener(OnbeatDetected);
            _ComboMovement.ResetVariables -= ResetMovement;
            _ComboMovement.ComboFilled -= PlayCombo;
        }

        private void Update()
        {
            // Shoot();
            SetAnimationVariables();
            _ComboMovement.MoveOnBeat(_OnBeating, _Move);
            
            if (GameController.Instance.players[(int)_Player].health == 0)
			{
				GameController.Winner = _Player == Player.Player2 ? "Player 1" : "Player 2";
				SceneManager.LoadScene("GameOverScene"); 
			}
        }

        private void FixedUpdate()
        {
            MoveSprite();
        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("Pit"))
            {
                --GameController.Instance.players[(int) _Player].health;
            }
        }
    #endregion UnityFunctions

    #region ClassMethods

        public void Initialize(PlayerSetup setup) 
        {
            _Player = setup._Player;
            _PlayerInput = setup._PlayerInput;
            _OSSytem = setup._OSSytem;
        }
        
        public void OnbeatDetected()
        {
            if (GameController.Instance.isReady)
            {
                StartCoroutine(EnabletoMove());       
            }
        }

        private IEnumerator EnabletoMove()
        {
            _Move = false;
            _Shoot = false;
            Vector3 posBeforeBeat = transform.position;
            EnableInput(true);
            _OnBeating = true;
            _ComboMovement.MoveOnBeat(_OnBeating, _Move);
            yield return new WaitForSeconds(_WindowToMove);
            if (posBeforeBeat == transform.position) 
            {
                _ComboMovement.ResetMoveCount();
            }
            _OnBeating = false;
            EnableInput(false);
        }

        private void ResetMovement(bool beat, bool movement)
        {
            _OnBeating = beat;
            _Move = movement;
        }

        public void onMoveRight()
        {
            SetFaceSprite(1, 0, SpriteType.Move);
        }

        public void onMoveLeft()
        {
            SetFaceSprite(-1, 0, SpriteType.Move);
        }

        public void onMoveUp()
        {
            SetFaceSprite(0, 1, SpriteType.Move);
        }

        public void onMoveDown()
        {
            SetFaceSprite(0, -1, SpriteType.Move);
        }

        public void OnShootLeft()
        {
            SetFaceSprite(-1, 0, SpriteType.Shoot);
        }
        
        public void OnShootRight()
        {
            SetFaceSprite(1, 0, SpriteType.Shoot);
        }
        public void OnShootDown()
        {
            SetFaceSprite(0, -1, SpriteType.Shoot);
        }
        public void OnShootUp()
        {
            SetFaceSprite(0, 1, SpriteType.Shoot);
        }

        private void SetFaceSprite(float faceX, float faceZ, SpriteType type)
        {
            if (type == SpriteType.Move)
            {
                _Move = true;
            }
            else if (type == SpriteType.Shoot)
            {
                _Shoot = true;
            }
            _FacingX = faceX;
            _FacingZ = faceZ;
        }

        private void MoveSprite()
        {
            _Sprite.transform.position = Vector3.SmoothDamp(_Sprite.transform.position, _SpriteAnchor.transform.position, ref _CurrentVelocity, 0.1f, 10f);
        }

        private void SetAnimationVariables()
        {
            if(_Move)
            {
                _Anim.SetFloat("DirectionX", _FacingX);
                _Anim.SetFloat("DirectionZ", _FacingZ);
                if(_OnBeating)
                {
                    _Anim.SetTrigger("Spin");
                }
            }

            if(_Shoot)
            {
                _Anim.SetTrigger("Shoot");
                _Anim.SetFloat("DirectionX", _FacingX);
                _Anim.SetFloat("DirectionZ", _FacingZ);
            }
        }

        private void PlayCombo()
        {   
            var clonb = Instantiate (_ComboPS, _Sprite.transform.position, _ComboPS.transform.rotation);

            clonb.transform.SetParent(_Sprite.transform);
            
            int index = UnityEngine.Random.Range (0, _ComboSounds.Length);
            _ComboSound = _ComboSounds[index];
           
           if(_Player == Player.Player1)
           {
               _ComboSound = _ComboSounds[index];
           }
           else
           {
               _ComboSound = _SquinkleSounds[index];

           }
            _PCAudio.clip = _ComboSound;
            _PCAudio.Play();            
        }
	#endregion ClassMethods
}
