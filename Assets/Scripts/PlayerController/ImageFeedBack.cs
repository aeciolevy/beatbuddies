﻿using UnityEngine;
using UnityEngine.UI;

public class ImageFeedBack : MonoBehaviour 
{
	#region GlobalVariables
		
		[SerializeField]
		private float _Speed = 1.0f;
		
		[SerializeField]
		private float _MoveRange = 1.5f;

		[SerializeField]
		private float _SpriteSize = 1f;
		
		private float _newYPosition;
		private float _newZPosition;
		
		private CameraController _GameCamera;

	#endregion GlobalVariables

	#region LifeCycle
		
		private void Start()
		{
			_GameCamera = FindObjectOfType<GameCamera>().gameObject.GetComponentInParent<CameraController>();
			_SpriteSize = Mathf.Abs(_GameCamera._cameraPosition.z / 15);
			float moveDistance = Mathf.Abs(_MoveRange * _GameCamera._cameraPosition.z);
			_newYPosition = transform.position.y + moveDistance;
			_newZPosition = transform.position.z + moveDistance;
			transform.Rotate(45, 0 ,0);
		}

		private void Update()
		{
			float newY = Mathf.Lerp(transform.position.y, _newYPosition, Time.deltaTime * _Speed);
			float newZ = Mathf.Lerp(transform.position.z, _newZPosition, Time.deltaTime * _Speed);
			_SpriteSize = Mathf.Abs(_GameCamera._cameraPosition.z / 15);

			transform.position = new Vector3(transform.position.x, newY, newZ);
			transform.localScale = new Vector3(_SpriteSize, _SpriteSize, _SpriteSize);
		}

	#endregion LifeCycle
	
}
