﻿
using System;
using System.Collections;
using UnityEngine;

public class ComboMovement 
{
	#region GlobalVariables

		private int _MovementCount;
		private int _Bullets;
		private int _Player;
		private bool _Moved;
		private bool _OnBeat;
		public  bool _IsOverBridge;
		public event Action<bool, bool> ResetVariables = delegate { };
		public event Action ComboFilled = delegate {};
		public event Action BulletMax = delegate { };
		public event Action BulletDouble = delegate { };
	#endregion GlobalVariables

	public ComboMovement(int movement, int bullet, int player)
	{
		_MovementCount = movement;
		_Bullets = bullet;
		_Player = player;
	}
	#region MethosAndProperties
		public int MovementOnBeat 
		{
			get 
			{
				return _MovementCount;
			}
			set 
			{
				_MovementCount = value;
			}
		}

		public int Bullets
		{
			get 
			{
				return _Bullets;
			}
			set
			{
				_Bullets = value;
			}
		}

		public void MoveOnBeat(bool onBeat, bool isMoved)
		{
			if (onBeat == true && isMoved == true)
			{
				++_MovementCount;
				GameController.Instance.players[_Player].combo++;
				ResetVariables(false, false);
			} 
			// if try to move of beat, reset the count
			else if (onBeat == false && isMoved == true)
			{
				_MovementCount = 0;
				GameController.Instance.players[_Player].combo = 0;
			}
			if (_MovementCount == GameController.Instance.MOVEMENTS_TO_EARN_BULLET) 
			{
				// Reset Movement Count if the player reach the movement needed to earn bullet
				_MovementCount = 0;
				// if is less than max bullets add one more
				if (_Bullets < GameController.Instance.MAX_BULLETS)
				{
					
					// call Action combo filled;
					if (_IsOverBridge)
					{
						_Bullets += 2;
						BulletDouble();
						_IsOverBridge = false;
						ComboFilled();
					}
					else 
					{
						ComboFilled();
						++_Bullets;
					}
					
				}
				// if bullet is equal to max, call action to show feedback to player
				else if (_Bullets == GameController.Instance.MAX_BULLETS)
				{
					BulletMax();
				}
				GameController.Instance.players[_Player].bullets = _Bullets; 
				GameController.Instance.players[_Player].combo = 0; 
			}
			// if movement count is not equal to the movements to earn bullet always reset bridge
			else 
			{
				_IsOverBridge = false;
			}
		}

		public void ResetMoveCount()
		{
			_MovementCount = 0;
		}
	#endregion MethosAndProperties
}
