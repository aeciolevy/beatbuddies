﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpriteFeedback
{
    public Sprite Sprite;
    public PlayerEventTypes Action;
}

