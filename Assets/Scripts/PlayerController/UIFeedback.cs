﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFeedback : MonoBehaviour 
{

	#region GlobalVariables
		
		
		[SerializeField]
		private SpriteRenderer _Sprite;

		[Tooltip("Time to destroy the sprite")]
		[SerializeField]
		private float _TimeToDestroy = 1.0f;

		[SerializeField]
		private List<SpriteFeedback> _EventsActions = new List<SpriteFeedback>();
		
		private Dictionary<PlayerEventTypes, Sprite> _SpriteSelected = new Dictionary<PlayerEventTypes, Sprite>();

		private PlayerController _PlayerController;

		private Shooter _Shooter;

		private Player _Owner;
		private AudioSource _AudioS;
		[SerializeField]
		private AudioClip _DryFire;

	#endregion GlobalVariables
	
	#region LifeCycle

		

		private void Start()
		{
			_AudioS = GetComponent<AudioSource>();
			_PlayerController = GetComponentInParent<PlayerController>();
			_PlayerController._ComboMovement.ComboFilled += EarnBullet;
			_PlayerController._ComboMovement.BulletMax += MaxBulletWarn;
			_PlayerController._ComboMovement.BulletDouble += DoubleBullet;
			_Shooter = GetComponentInParent<Shooter>();
			_Shooter.BulletZero += ZeroBullet;
			foreach (var item in _EventsActions)
			{
				AssignEvents(item);
			}
			
		}

		void OnDisable()
		{
			_PlayerController._ComboMovement.ComboFilled -= EarnBullet;
		}
    #endregion LifeCycle

    #region ClassMethods

		void AssignEvents(SpriteFeedback eventAction)
		{
			switch (eventAction.Action)
			{
				case PlayerEventTypes.EarnBullet:
					{
						_SpriteSelected.Add(PlayerEventTypes.EarnBullet, eventAction.Sprite);
						break;
					}
				case PlayerEventTypes.ZeroBullet:
					{
						
						_SpriteSelected.Add(PlayerEventTypes.ZeroBullet, eventAction.Sprite);
						break;
					}
				case PlayerEventTypes.MaxBullet:
					{
						_SpriteSelected.Add(PlayerEventTypes.MaxBullet, eventAction.Sprite);
						break;
					}
				case PlayerEventTypes.DoubleBullet:
					{
						_SpriteSelected.Add(PlayerEventTypes.DoubleBullet, eventAction.Sprite);
						break;
					}
				default:
					break;
			}
		}

		private void EarnBullet()
		{
			// Instatiate Image and destroy t seconds later
			_Sprite.sprite = _SpriteSelected[PlayerEventTypes.EarnBullet];
			var image = Instantiate(_Sprite, transform.position, Quaternion.identity);
			Destroy(image.gameObject, _TimeToDestroy);

		}

		private void ZeroBullet()
		{
			_AudioS.clip = _DryFire;
			_AudioS.Play();
			_Sprite.sprite = _SpriteSelected[PlayerEventTypes.ZeroBullet];
			var image = Instantiate(_Sprite, transform.position, Quaternion.identity);
			Destroy(image.gameObject, _TimeToDestroy);
		}

		private void MaxBulletWarn()
		{
			// Show sprite of max bullets
			_Sprite.sprite = _SpriteSelected[PlayerEventTypes.MaxBullet];
			var image = Instantiate(_Sprite, transform.position, Quaternion.identity);
			Destroy(image.gameObject, _TimeToDestroy);
		}


		private void DoubleBullet()
		{
			// Show sprite of max bullets
			_Sprite.sprite = _SpriteSelected[PlayerEventTypes.DoubleBullet];
			var image = Instantiate(_Sprite, transform.position, Quaternion.identity);
			Destroy(image.gameObject, _TimeToDestroy);
		}
	#endregion

}
