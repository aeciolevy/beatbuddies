﻿using System;
using UnityEngine;

public class Shooter : MonoBehaviour 
{
	#region GlobalVariables
		[SerializeField]
		private Projectile _ProjectilePrefab;
		[SerializeField]
		private Transform _BulletSpawnLeft;
		[SerializeField]
		private Transform _BulletSpawnRight;
		[SerializeField]
		private Transform _BulletSpawnFoward;
		[SerializeField]
		private Transform _BulletSpawnBackward;

		[Header("Input to Shoot")]
		[SerializeField]
		private KeyCode _ShootUp = KeyCode.W;

		[SerializeField]
		private KeyCode _ShootDown = KeyCode.S;

		[SerializeField]
		private KeyCode _ShootLeft = KeyCode.A;

		[SerializeField]
		private KeyCode _ShootRight = KeyCode.D;

		[Header("Events")]
		[SerializeField]
		private ShootLeftEvent ShootLeft;
		[SerializeField]
		private ShootRightEvent ShootRight;
		[SerializeField]
		private ShootUpEvent ShootUp;
		[SerializeField]
		private ShootDownEvent ShootDown;

		private Player _Player;	
		private PlayerInputType _PlayerInput;	
		private OSSystem _OSSytem;

		private bool _EnableToShoot;
		private int _Bullets;
		private bool _Shoot;

		private PlayerController _Owner;
		private ComboMovement _ComboController;
		[SerializeField]
		private AudioSource _ShootSoundSource;
		[SerializeField]
		internal AudioClip _ShootSound;
		[SerializeField]
		internal AudioClip _DryFire;

		internal event Action BulletZero = delegate { };

    #endregion GlobalVariables

    #region LifeCycle
		private void Awake()
		{
			var player = GetComponent<PlayerController>();
			player.EnableInput += isEnableInput;
			_Owner = GetComponent<PlayerController>();	
		}

		private void Start()
		{
			if (_Owner != null)
			{
				_Player = _Owner._Player;
				_PlayerInput = _Owner._PlayerInput;
				_OSSytem = _Owner._OSSytem;
			}
			_ComboController = _Owner._ComboMovement;
		}

		private void Update()
		{
			Shoot();
		}
    #endregion LifeCycle

	private void isEnableInput(bool onBeat)
	{
		_EnableToShoot = onBeat;
	}

    #region ClassMethods
    private void Shoot()
		{
			_Bullets = _ComboController.Bullets;
			if (_EnableToShoot)
			{
				bool shootLeft = Input.GetKeyDown(_ShootLeft);
				bool shootRight = Input.GetKeyDown(_ShootRight);
				bool shootUp = Input.GetKeyDown(_ShootUp);
				bool shootDown = Input.GetKeyDown(_ShootDown);
				string suffixOS = _OSSytem == OSSystem.Windows ? "" : "Mac";
				string suffixPlayer = _Player == Player.Player1 ? "" : "2";

				if (_PlayerInput == PlayerInputType.Joystick)
				{
					shootLeft = Input.GetButtonDown("XButton" + suffixOS + suffixPlayer);
					shootRight = Input.GetButtonDown("BButton" + suffixOS + suffixPlayer);
					shootUp = Input.GetButtonDown("YButton" + suffixOS + suffixPlayer);
					shootDown = Input.GetButtonDown("AButton" + suffixOS + suffixPlayer);
				}

				if (shootLeft || shootRight || shootUp || shootDown)
				{
					if (_Bullets == 0)
					{
						BulletZero();
						return;
					}
					// Update UI
					--GameController.Instance.players[(int)_Player].bullets;
					// Update Player Bullets
					--_ComboController.Bullets;
					_ShootSoundSource.clip = _ShootSound;
					_ShootSoundSource.Play();
				}

				if (shootUp)
				{
					Instantiate(_ProjectilePrefab, _BulletSpawnFoward.position, _BulletSpawnFoward.localRotation).Shoot(0, 1, _Owner);
					ShootUp.Invoke();
				}
				if (shootDown)
				{
					Instantiate(_ProjectilePrefab, _BulletSpawnBackward.position, _BulletSpawnBackward.localRotation).Shoot(0, -1, _Owner);
					ShootDown.Invoke();
				}
				if (shootLeft)
				{
					Instantiate(_ProjectilePrefab, _BulletSpawnLeft.position, _BulletSpawnLeft.localRotation).Shoot(-1, 0, _Owner);
					ShootLeft.Invoke();
				}
				if (shootRight)
				{
					Instantiate(_ProjectilePrefab, _BulletSpawnRight.position, _BulletSpawnRight.localRotation).Shoot(1, 0, _Owner);
					ShootRight.Invoke();
				}
			}
			// else if (_EnableToShoot && _Bullets <= 0 || !_EnableToShoot && _Bullets <= 0)
			// else if (_Bullets <= 0 || !_EnableToShoot)
			// {
			// 	string suffixOS = _OSSytem == OSSystem.Windows ? "" : "Mac";
			// 	string suffixPlayer = _Player == Player.Player1 ? "" : "2";

			// 	bool Shooting = Input.GetButtonDown("XButton" + suffixOS + suffixPlayer) || Input.GetButtonDown("BButton" + suffixOS + suffixPlayer) || Input.GetButtonDown("YButton" + suffixOS + suffixPlayer) || Input.GetButtonDown("AButton" + suffixOS + suffixPlayer);
			// 	if(Shooting)
			// 	{
			// 		_ShootSoundSource.clip = _DryFire;
			// 		_ShootSoundSource.Play();
			// 	}

			// }
			// else(_)
		}
    #endregion ClassMethods

    #region EventsClasses
    [System.Serializable]
    public class ShootRightEvent : UnityEngine.Events.UnityEvent { }

    [System.Serializable]
    public class ShootLeftEvent : UnityEngine.Events.UnityEvent { }

    [System.Serializable]
    public class ShootUpEvent : UnityEngine.Events.UnityEvent { }

    [System.Serializable]
    public class ShootDownEvent : UnityEngine.Events.UnityEvent { }
    #endregion EventsClasses

}
