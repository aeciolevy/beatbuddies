﻿using System.Collections;
using UnityEngine;

// Script by Paul Hanberry
// Makes the title appear in time with brendan's announcer VO
// Then makes the buttons appear and starts the music
public class TitleIntroUI : MonoBehaviour 
{
	[SerializeField]
	private GameObject _BattleTitle;
	[SerializeField]
	private GameObject _BeatTitle;
	[SerializeField]
	private GameObject _BuddiesTitle;
	[SerializeField]
	private GameObject _FullTitle;
	[SerializeField]
	private GameObject _Buttons;

	[SerializeField]
	private AudioSource _TitleYell;
	[SerializeField]
	private AudioSource _Music;

	private bool _IntroPlaying = true;
	private bool _ButtonsLerp = false;

	private Vector3 _Vel;
	private Coroutine IntroCoroutine;

	void Start ()
	{
		_IntroPlaying = true;
		StartCoroutine(TitleIntro());
	}
	
	private void FixedUpdate()
	{
		if(_ButtonsLerp)
		{
			_Buttons.transform.position = Vector3.SmoothDamp(_Buttons.transform.position, this.gameObject.transform.position, ref _Vel, 1, 500);
		}
		if(Input.anyKeyDown && _IntroPlaying)
		{
			_Buttons.SetActive(true);
			_Buttons.transform.position = this.transform.position;
		}
	}

	// Where the magic happens
	IEnumerator TitleIntro()
	{
		while(_IntroPlaying)
		{
			// Turn everything off in case enabled in editor
			_BattleTitle.SetActive(false);
			_BeatTitle.SetActive(false);
			_BuddiesTitle.SetActive(false);
			_FullTitle.SetActive(false);
			

			yield return new WaitForSeconds(1f);
			
			_Buttons.SetActive(false);

			// Begin intro sequence
			// BATTLE!
			_TitleYell.Play();
			_BattleTitle.SetActive(true);
			
			yield return new WaitForSeconds(1.2f);

			// BEAT!
			_BeatTitle.SetActive(true);

			yield return new WaitForSeconds(1.4f);

			_IntroPlaying = false;
		}
		StartCoroutine(TitleIntroFinish());

		yield return null;
	}

	IEnumerator TitleIntroFinish()
	{
		// BUDDIES!
		// Reveal full title and buttons
		_BuddiesTitle.SetActive(true);
		_Buttons.SetActive(true);

		// Hide the single-word titles titles

		// Play music and bring the buttons into view
		_Music.Play();

		_ButtonsLerp = true;

		yield return new WaitForSeconds(2.3f);

		_FullTitle.SetActive(true);

		_BattleTitle.SetActive(false);
		_BeatTitle.SetActive(false);
		_BuddiesTitle.SetActive(false);

		yield return null;
	}
}