﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverUI : MonoBehaviour 
{

  #region GlobalVariables

		[SerializeField]
		private GameObject _RotationText;
		[SerializeField]
		private GameObject _P1Sprite;
		[SerializeField]
		private GameObject _P2Sprite;
		
  #endregion GlobalVariables

	#region UnityFunctions

    void Start () 
	{
		if(GameController.Winner == "Player 1" )
		{
			_RotationText.GetComponent<TextLadder>()._Message = "Disco Dan Wins!";
			_P1Sprite.SetActive(true);
			_P2Sprite.SetActive(false);
		}
		if(GameController.Winner == "Player 2")
		{
			_RotationText.GetComponent<TextLadder>()._Message = "Squinkle Wins!";
			_P2Sprite.SetActive(true);
			_P1Sprite.SetActive(false);
		}	
	}

	#endregion UnityFunctions

	#region ClassFuntions

		public void MainMenu()
		{
			SceneManager.LoadScene("MainMenu");
		}

	#endregion ClassFuntions

}
