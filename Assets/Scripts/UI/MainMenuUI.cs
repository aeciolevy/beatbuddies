﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Script by Paul Hanberry
// Gives functionality to the main menu
public class MainMenuUI : MonoBehaviour
{
	#region ClassVariables

		// MAKE SURE the string that's entered matches the scene title perfectly
		[Header("On Play Scene Load")]
		[SerializeField]
		private List<string> _LevelScene;

		[Header("Anchors")]
		[SerializeField]
		private Transform _PanelAnchor;
		[SerializeField]
		private Transform _MainMenuAnchor;
		[SerializeField]
		private Transform _ControlsAnchor;
		[SerializeField]
		private Transform _CreditsAnchor;
		[SerializeField]
		private Transform _LevelSelectAnchor;
		[SerializeField]
		private Transform _ThanksAnchor;

		[Header("Panels")]
		[SerializeField]
		private GameObject _MainMenuPanel;
		[SerializeField]
		private GameObject _ControlsPanel;
		[SerializeField]
		private GameObject _CreditsPanel;
		[SerializeField]
		private GameObject _LevelSelectPanel;
		[SerializeField]
		private GameObject _ThanksPanel;

		[Header("Sounds")]
		[SerializeField]
		private AudioSource _SelectSound;
		[SerializeField]
		private AudioSource _MoveSound;
		[SerializeField]
		private AudioSource _GrooveSound;

		private int _PanelNumber = 1;
		private Vector3 _CurrentVelocity;
		private Vector3 _PanelAnchorTarget;

	#endregion ClassVariables

	#region UnityFunctions

		private void Start()
		{
			// Sets the panel movement system to its default state
			_PanelAnchorTarget = _MainMenuAnchor.transform.position;
			StartCoroutine(Wait(1));
		}

		private void Update()
		{
			// The panel  anchor will always lerp towards whatevers set as it's target.
			// By changing the value of the target, we can easily move the panels around.
			_PanelAnchor.transform.position = Vector3.SmoothDamp(_PanelAnchor.transform.position, _PanelAnchorTarget, ref _CurrentVelocity, 0.5f, 1000);

			if(_MainMenuPanel.activeInHierarchy == false 
				&& _ControlsPanel.activeInHierarchy == false 
				&& _CreditsPanel.activeInHierarchy == false 
				&& _LevelSelectPanel.activeInHierarchy == false
				&& _ThanksPanel.activeInHierarchy == false)
			{
				ResetPanels(1);
			}
		}

	#endregion UnityFunctions

	#region ClassFunctions

		public void Play(int level)
		{
			// Loads the gameplay scene
			_SelectSound.Play();
			SceneManager.LoadScene(_LevelScene[level]);
		}

		public void Groove()
		{
			_GrooveSound.Play();
		}

		public void Quit()
		{
			// Kills the application if using exe, stops editor playback if in editor
			_SelectSound.Play();
			Application.Quit();
		}

		public void Move()
		{
			_MoveSound.Play();
		}

		// Transitions to the panel indicated by the panel number
		// PANEL 0: Controls Screen
		// PANEL 1: Main Screen
		// PANEL 2: Credits Screen
		// PANEL 3: Level Select
		// PANEL 4: Special Thanks
		public void Transition(int panel)
		{
			_SelectSound.Play();
			StartCoroutine(Wait(panel));
		}

		private void ResetPanels(int panel)
		{
			StartCoroutine(Wait(panel));
		}

		// Sets a new target for the panel anchor, enables active panel, waits, then disables unused pan
		IEnumerator Wait(int panel)
		{
			switch (panel)
			{
				case 0:

					_ControlsPanel.SetActive(true);
					_PanelAnchorTarget = _ControlsAnchor.position;

					yield return new WaitForSeconds(1);
					
					_MainMenuPanel.SetActive(false);
					_CreditsPanel.SetActive(false);
					_LevelSelectPanel.SetActive(false);
					_ThanksPanel.SetActive(false);

					break;

				case 1:

					_MainMenuPanel.SetActive(true);
					_PanelAnchorTarget = _MainMenuAnchor.position;

					yield return new WaitForSeconds(1);
					
					_ControlsPanel.SetActive(false);
					_CreditsPanel.SetActive(false);
					_LevelSelectPanel.SetActive(false);
					_ThanksPanel.SetActive(false);

					break;

				case 2:

					_CreditsPanel.SetActive(true);
					_PanelAnchorTarget = _CreditsAnchor.position;

					yield return new WaitForSeconds(1);
					
					_ControlsPanel.SetActive(false);
					_MainMenuPanel.SetActive(false);
					_LevelSelectPanel.SetActive(false);
					_ThanksPanel.SetActive(false);

					break;

				case 3:
					
					_LevelSelectPanel.SetActive(true);
					_PanelAnchorTarget = _LevelSelectAnchor.position;

					yield return new WaitForSeconds(1);

					
					_ControlsPanel.SetActive(false);
					_MainMenuPanel.SetActive(false);
					_CreditsPanel.SetActive(false);
					_ThanksPanel.SetActive(false);

					break;

				case 4:

					_ThanksPanel.SetActive(true);
					_PanelAnchorTarget = _ThanksAnchor.position;

					yield return new WaitForSeconds(1);

					_ControlsPanel.SetActive(false);
					_MainMenuPanel.SetActive(false);
					_CreditsPanel.SetActive(false);
					_LevelSelectPanel.SetActive(false);
					
					break;

				default:

					break;

			
			}
		}

	#endregion ClassFunctions
}
