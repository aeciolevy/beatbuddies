﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

// Script by Paul Hanberry
// With help from Willy Campos
// Place this script onto the default button on a panel
// It will move controlelr navigation to the button
public class ForceButtonSelected : MonoBehaviour
{
	private void OnEnable()
	{
		StartCoroutine(HighlightButton());
	}
	
	IEnumerator HighlightButton()
	{
		yield return new WaitForSecondsRealtime(0.1f);
		GetComponent<Button>().Select();
	}
}
