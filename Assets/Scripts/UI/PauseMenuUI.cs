﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuUI : MonoBehaviour
{
	#region ClassVariables

		[SerializeField]
		private GameObject _PausePanel;
		[SerializeField]
		private GameObject _FloatingText;

		private TextLadder _TextLadder;

		private bool _Paused;
		
	#endregion ClassVariables

	#region UnityFunctions
		
		private void Start()
		{
			_TextLadder = _FloatingText.GetComponent<TextLadder>();	
		}

		void Update () 
		{
			if(Input.GetButtonDown("Pause"))
			{
				if(GameController.Instance._IsPaused)
				{
					Unpause();
				}
				else
				{
					Pause();
				}
			}
		}

	#endregion UnityFunctions

	#region ClassFunctions

		public void Pause()
		{
			_Paused = true;
			StartCoroutine(FakeUpdate());
			Time.timeScale = 0;
			_PausePanel.SetActive(true);
			GameController.Instance._IsPaused = true;
		}

		public void Unpause()
		{
			_Paused = false;
			Time.timeScale = 1;
			_PausePanel.SetActive(false);
			GameController.Instance._IsPaused = false;
		}

		public void MainMenu()
		{
			Time.timeScale = 1;
			SceneManager.LoadScene("MainMenu");
		}

		public void Reload()
		{
			Time.timeScale = 1;
			Scene _Scene = SceneManager.GetActiveScene();
			SceneManager.LoadScene(_Scene.name);
		}

		public void Quit()
		{
			Application.Quit();
		}

		public IEnumerator FakeUpdate()
		{
			while(_Paused)
			{
				yield return new WaitForSecondsRealtime(0.0166f);
				_TextLadder.FakeUpdate();
			}
			yield return null;
		}

	#endregion ClassFunctions
}
