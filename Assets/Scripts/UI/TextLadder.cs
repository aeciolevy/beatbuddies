﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Script by Paul Hanberry
// Creates 4 texts and lines them up behind each other betewen 2 points
// Designer can choose to have them rotate around a fixed point
public class TextLadder : MonoBehaviour
{
	#region ClassVariables

		[Header("Anchors")]
		[SerializeField]
		private GameObject _RotationBaseAnchor;
		[SerializeField]
		private GameObject _BottomAnchor;
		[SerializeField]
		private GameObject _TargetAnchor;

		[Header("Text")]
		[SerializeField]
		public string _Message = ("[Put Text Here]");
		[SerializeField]
		private bool _Wiggle = false;
		[SerializeField]
		private GameObject _Image1;
		[SerializeField]
		private GameObject _Image2;
		[SerializeField]
		private GameObject _Image3;
		[SerializeField]
		private GameObject _Image4;

		private Vector3 _Vel;


	#endregion ClassVariables

	#region UnityFunctions

		// If the provided gameobjects are texts, set the message to equal the string entered by the designer
		private void Start ()
		{
			if(_Image1.GetComponent<Text>() != null)
			{
				_Image1.GetComponent<Text>().text = _Message;
				_Image2.GetComponent<Text>().text = _Message;
				_Image3.GetComponent<Text>().text = _Message;
				_Image4.GetComponent<Text>().text = _Message;
			}
		}

		private void Update()
		{
			UpdatePositions();
			CalculatePositions();
		}

    #endregion UnityFunctions

    #region ClassFuncions

		public void FakeUpdate()
		{
			CalculatePositions();
			FakeUpdatePositions();
		}

		// Sets the images to be 1/4, 1/2, and 3/4 of the way between the bottom anchor and the top image
		private void CalculatePositions()
		{
			_Image2.transform.position = (_Image1.transform.position * 3 + _BottomAnchor.transform.position)/4;
			_Image3.transform.position = (_Image1.transform.position + _BottomAnchor.transform.position)/2;
			_Image4.transform.position = (_Image1.transform.position + _BottomAnchor.transform.position * 3)/4;
		}

		private void UpdatePositions()
		{
			// SmoothDamps the top image towards the target
			_Image1.transform.position = Vector3.SmoothDamp(_Image1.transform.position, _TargetAnchor.transform.position, ref _Vel, 1, 3000);
			
			// If enabled in the editor, move the text in a circle
			if(_Wiggle)
			{
				_RotationBaseAnchor.transform.Rotate(Vector3.back);
			}
		}

		private void FakeUpdatePositions()
		{
			_Image1.transform.position = _TargetAnchor.transform.position;
			
			if(_Wiggle)
			{
				_RotationBaseAnchor.transform.Rotate(Vector3.back * 0.6f);
			}
		}
		
	#endregion ClassFunctions
}
