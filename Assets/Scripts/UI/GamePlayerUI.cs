﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayerUI : MonoBehaviour 
{
	#region GlobalVariables
		[Header("Player1")]
		[Header("DisplayImages")]
		[SerializeField]
		private Image _HUDFrameP1;
		[SerializeField]
		private Image _HealthDisplayP1;
		[SerializeField]
		private Image _ComboDisplayP1;
		[SerializeField]
		private Image _BulletsDisplayP1;
		
		[Header("Player2")]
		[Header("DisplayImages")]
		[SerializeField]
		private Image _HUDFrameP2;
		[SerializeField]
		private Image _HealthDisplayP2;
		[SerializeField]
		private Image _ComboDisplayP2;
		[SerializeField]
		private Image _BulletsDisplayP2;

		[Header("SourceImages")]
		[SerializeField]
		private List <Sprite> _Health = new List<Sprite>();
		[SerializeField]
		private List <Sprite> _Combo = new List<Sprite>();
		[SerializeField]
		private List <Sprite> _Bullets = new List<Sprite>();

		[Header("GameUI")]
		[SerializeField]
		private Image _Panel;
		[SerializeField]
		private List <GameObject> _CountdownNumbers;
		[SerializeField]
		private List <AudioClip> _CountdownVO;
		[SerializeField]
		private Text _CountDown;
		[SerializeField]
		private float _TimeBetweenCountdown = 1f;

		[SerializeField]
		private float _CountDownTimer = 3.0f;

	
		private int _CountdownTick = 3;
		
		private Color _P1Transparent;
		private Color _P2Transparent;

		private Color _P1OpaqueColor;
		private Color _P2OpaqueColor;

		private Color _OpaqueBlank;

		private AudioSource _AudioSource;

	#endregion GlobalVariables

	#region LifeCycle

		private void Start()
		{
			_P1Transparent = GameController.Instance._P1Color;
			_P1Transparent.a = 0;
			_P1OpaqueColor = GameController.Instance._P1Color;
			_P1OpaqueColor.a = 255;
			_HUDFrameP1.color = _P1OpaqueColor;

			_P2Transparent = GameController.Instance._P2Color;
			_P2Transparent.a = 0;
			_P2OpaqueColor = GameController.Instance._P2Color;
			_P2OpaqueColor.a = 255;
			_HUDFrameP2.color = _P2OpaqueColor;

			_OpaqueBlank = new Color (1, 1, 1, 1);

			_HealthDisplayP1.sprite = _Health[GameController.Instance.players[(int) Player.Player1].health - 1];
			_ComboDisplayP1.sprite = _Combo[0];
			_BulletsDisplayP1.sprite = _Bullets[0];

			_HealthDisplayP2.sprite = _Health[GameController.Instance.players[(int)Player.Player2].health - 1];
			_ComboDisplayP2.sprite = _Combo[0];
			_BulletsDisplayP2.sprite = _Bullets[0];

			_CountDown.enabled = false;
			GameController.CountDown = _CountDownTimer;

			_AudioSource = GetComponentInChildren<AudioSource>();

			StartCoroutine(TimedCountdown());
		}

		private void Update()
		{
			// Show countdown in the UI
			if (GameController.CountDown > 0)
			{
				ShowCountDown();
			}
			
			// P1 Health Display
			if(GameController.Instance.players[(int) Player.Player1].health <= 0)
			{
				_HealthDisplayP1.sprite = null;
				_HealthDisplayP1.color = _P1Transparent;
			}
			else if(GameController.Instance.players[(int) Player.Player1].health >= 4)
			{
				_HealthDisplayP1.sprite = _Health[2];
				_HealthDisplayP1.color = _OpaqueBlank;
			}
			else
			{
				_HealthDisplayP1.sprite = _Health[GameController.Instance.players[(int) Player.Player1].health - 1];
				_HealthDisplayP1.color = _OpaqueBlank;

			}

			// P1 Combo Display
			if(GameController.Instance.players[(int) Player.Player1].combo <= 0)
			{
				_ComboDisplayP1.sprite = null;
				_ComboDisplayP1.color = _P1Transparent;
			}
			else if(GameController.Instance.players[(int) Player.Player1].combo >= 4)
			{
				_ComboDisplayP1.sprite = _Combo[2];
				_ComboDisplayP1.color = _OpaqueBlank;
			}
			else
			{
				_ComboDisplayP1.sprite = _Combo[GameController.Instance.players[(int) Player.Player1].combo - 1];
				_ComboDisplayP1.color = _OpaqueBlank;
			}

			// P1 Bullets Display
			if(GameController.Instance.players[(int) Player.Player1].bullets <= 0)
			{
				_BulletsDisplayP1.sprite = null;
				_BulletsDisplayP1.color = _P1Transparent;
			}
			else if(GameController.Instance.players[(int) Player.Player1].bullets >= 11)
			{
				_BulletsDisplayP1.sprite = _Bullets[9];
				_BulletsDisplayP1.color = _OpaqueBlank;
			}
			else
			{
				_BulletsDisplayP1.sprite = _Bullets[GameController.Instance.players[(int) Player.Player1].bullets - 1];
				_BulletsDisplayP1.color = _OpaqueBlank;
			}

			// P2 Health Display
			if(GameController.Instance.players[(int) Player.Player2].health <= 0)
			{
				_HealthDisplayP2.sprite = null;
				_HealthDisplayP2.color = _P2Transparent;
			}
			else if(GameController.Instance.players[(int) Player.Player2].health >= 4)
			{
				_HealthDisplayP2.sprite = _Health[2];
				_HealthDisplayP2.color = _OpaqueBlank;
			}
			else
			{
				_HealthDisplayP2.sprite = _Health[GameController.Instance.players[(int) Player.Player2].health - 1];
				_HealthDisplayP2.color = _OpaqueBlank;

			}

			// P2 Combo Display
			if(GameController.Instance.players[(int) Player.Player2].combo <= 0)
			{
				_ComboDisplayP2.sprite = null;
				_ComboDisplayP2.color = _P2Transparent;
			}
			else if(GameController.Instance.players[(int) Player.Player2].combo >= 4)
			{
				_ComboDisplayP2.sprite = _Combo[2];
				_ComboDisplayP2.color = _OpaqueBlank;
			}
			else
			{
				_ComboDisplayP2.sprite = _Combo[GameController.Instance.players[(int) Player.Player2].combo - 1];
				_ComboDisplayP2.color = _OpaqueBlank;
			}

			// P2 Bullet Display
			if(GameController.Instance.players[(int) Player.Player2].bullets <= 0)
			{
				_BulletsDisplayP2.sprite = null;
				_BulletsDisplayP2.color = _P2Transparent;
			}
			else if(GameController.Instance.players[(int) Player.Player2].bullets >= 11)
			{
				_BulletsDisplayP2.sprite = _Bullets[9];
				_BulletsDisplayP2.color = _OpaqueBlank;
			}
			else
			{
				_BulletsDisplayP2.sprite = _Bullets[GameController.Instance.players[(int) Player.Player2].bullets - 1];
				_BulletsDisplayP2.color = _OpaqueBlank;
			}
		}

    #endregion LifeCycle

    #region ClassMethods

		private void ShowCountDown() 
		{
			// CountDown is the size of the first track
			GameController.CountDown -= Time.deltaTime;
			if (_CountdownTick == 0) 
			// {
			// 	if (GameController.CountDown <= _CountDownTimer && GameController.CountDown > 0)
			// 	{
			// 		_CountDown.enabled = true;
			// 		--_CountDownTimer;
			// 	}
			// }
			// else 
			{
				_Panel.CrossFadeAlpha(0.0f, 1.2f, false);
				if (GameController.CountDown <= 0) 
				{
					_Panel.enabled = false;
					_CountDown.enabled = false;
					GameController.Instance.isReady = true;
				}
			}
		}

		// Displays the numbers in the countdown and plays the audio clips in the right order.
		IEnumerator TimedCountdown()
		{
			while(_CountdownTick >= 0)
			{
				foreach(GameObject num in _CountdownNumbers)
				{
					num.SetActive(false);
				}
				_CountdownNumbers[_CountdownTick].SetActive(true);

				_AudioSource.clip = _CountdownVO[_CountdownTick];
				_AudioSource.Play();

				_CountdownTick--;

				yield return new WaitForSeconds(_TimeBetweenCountdown);
			}

			foreach(GameObject num in _CountdownNumbers)
			{
				num.SetActive(false);
			}
		}
    #endregion ClassMethods
}
