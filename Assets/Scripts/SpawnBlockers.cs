﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBlockers : MonoBehaviour 
{
	#region GlobalVariables

	[SerializeField]
	private GameObject _Blockers;

	private bool _IsActive = false;

	#endregion GlobalVariables

	#region  UnityFunctions

	private void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Player") && !_IsActive)
		{
			_Blockers.SetActive(true);

		}
	}



	#endregion UnityFunctions



	
}
