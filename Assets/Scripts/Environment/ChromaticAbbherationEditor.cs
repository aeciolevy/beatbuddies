﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

// Script by Paul Hanberry
//
// This script controlls the Chromatic Abbheration of the Unity post-processing stack
// It creates a pulse around the edge of the screen to the beat
//
// HOW TO SET UP:
// Place Post-Proccessing Profile on camera object
// Place script on camera object
// Drop Post-Proccessing Profile into Profile serialized field
// Be sure that UI canvas has its Render Mode set to Screen Space - Camera
public class ChromaticAbbherationEditor : MonoBehaviour
{
	[Header("Post-Procesing Profile")]
	[SerializeField]
	private PostProcessingProfile _Profile;

	[Header("Behaviour")]
	[SerializeField]
	[Range(0f,1f)]
	private float _AbbherationMax = 1;
	[SerializeField]
	[Range(0f,1f)]
	private float _AbbherationMin = 0;
	[SerializeField]
	[Range(0f,1f)]
	private float _DimPerFrame = 0.1f;

	private ChromaticAberrationModel.Settings _AbbherationSettings;

	// The profiles settings cannot be edited directly, so a variable copies the current settings, alters them accordingly, and writes them back to the settings
	// The Chromatic abbherations intensity is reduced every frame
	void Update ()
	{
		_AbbherationSettings = _Profile.chromaticAberration.settings;

		if(_AbbherationSettings.intensity > _AbbherationMin)
		{
			_AbbherationSettings.intensity -= _DimPerFrame;
		}

		_Profile.chromaticAberration.settings = _AbbherationSettings;
	}

	// If there's a beat, reset the chrom. abb. intensity to full
	public void ScreenPulse()
	{
		_AbbherationSettings.intensity = _AbbherationMax;
		_Profile.chromaticAberration.settings = _AbbherationSettings;
	}
}
