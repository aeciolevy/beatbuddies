﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Script placed on to switches to switch the light bridges on and off
public class LightBridge : MonoBehaviour 
{
	#region GlobalVariables
	//Segments of light bridge controlled by this script
	[SerializeField]
	private List<GameObject> _BridgeSegments;
	//Pit edges controlled by this scritp
	[SerializeField]
	private List<GameObject> _PitFalls;
	[SerializeField]
	private GameObject _BridgeSounds;
	

	private int i;
	//bool for determining if the light bridge is on or not
	private bool BridgeActive = true;


	#endregion GlobalVariables

	#region UnityFunctions
	//Runs at the start to ensure the bridge is set to active
	private void Start()
	{
		BridgeActive = true;
		
	}
	//Runs the code to switch the light bridge on or off only when hit by a bullet
	private void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Bullet"))
		{
			BridgeSwitch();
		}
	}


	#endregion UnityEngine

	#region ClassFunctions

		private void BridgeSwitch()
		{	//If the bridge is currently on, switch all the bridge segments to off and activate the markers that cause characters to fall
			AudioSource bridgeSounds = Instantiate (_BridgeSounds, transform.position, Quaternion.identity).GetComponentInParent<AudioSource>();
			bridgeSounds.Play();
			Destroy(bridgeSounds.gameObject,bridgeSounds.clip.length);
			
			if(BridgeActive == true)
			{
				foreach(GameObject i in _BridgeSegments)
				{
					i.SetActive(false);
				}
				foreach(GameObject i in _PitFalls)
				{
					i.SetActive(true);
				}
				BridgeActive = false;
			}
			//If bridge is not active activate teh bridge segments and turn off the pit markers
			else if(BridgeActive == false)
			{
				foreach(GameObject i in _BridgeSegments)
				{
					i.SetActive(true);
				}

				foreach(GameObject i in _PitFalls)
				{
					i.SetActive(false);
				}
				BridgeActive = true;
			}
		}
	

	#endregion ClassFunctions
	
}
