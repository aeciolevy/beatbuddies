﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMessage : MonoBehaviour 
{
	#region GlobalVariables

	[SerializeField]
	private GameObject _Message;
	[SerializeField]
	private Transform _ActiveAnchor;
	[SerializeField]
	private Transform _InactiveAnchor;

	private bool _TextActive = false;


	private Vector3 _TargetAnchor;
	private Vector3 _Vel;

	#endregion GlobalVariables

	#region UnityFunctions

		private void Start()
		{
			_TargetAnchor = _InactiveAnchor.position;
			_Message.transform.position = _InactiveAnchor.position;
		}

		private void Update()
		{
			_Message.transform.position = Vector3.SmoothDamp(_Message.transform.position, _TargetAnchor, ref _Vel, 1, 100);
		}

		void OnTriggerStay(Collider other)
		{
			if(other.CompareTag("Player") && !_TextActive)
			{
				_TargetAnchor = _ActiveAnchor.position;
				_TextActive = true;
			}
			
		}

		private void OnTriggerExit(Collider other)
		{
			if(other.CompareTag("Player") && _TextActive)
			{
				_TargetAnchor = _InactiveAnchor.position;
				_TextActive = false;
			}
		}

		




	#endregion UnityFunctions

	
}
