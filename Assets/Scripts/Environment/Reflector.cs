﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reflector : MonoBehaviour 
{
	#region GlobalVars
		//Gets the direction we want the bullet to reflect to
		[SerializeField]
		private Transform _ReflectDirect;

	#endregion GlobalVars

	#region ClassFunctions
		//Function moves the bullet to the correct transform to reflect and ensures it is facing the right direction
		public void ChangeDirection(GameObject obj)
		{
			obj.transform.position = _ReflectDirect.position;
			obj.GetComponentInChildren<ProjectileChild>().transform.position = obj.transform.position;
			obj.transform.rotation = _ReflectDirect.rotation;	
			
		}
	#endregion ClassFunctions
}
