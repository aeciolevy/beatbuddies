﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundController : MonoBehaviour 
{
	#region GlobalVars
		[SerializeField]
		List<AudioClip> _Clips;

		[SerializeField]
		private ELoopType _LoopType;

		[SerializeField]
		private float _TrackOffsetLoad = 0.2f;

		private AudioSource _ASource;
		private int _ClipIndex = 0;
		
		private float _TrackDuration = 0;	
		private float _ASourcePitch;
		private float _ASourcePitchOld;

	#endregion GlobalVars

	#region UnityFunctions
		private void Start()
		{
			_ASource = GetComponent<AudioSource>();
			_ASourcePitch = _ASource.pitch;
			_ASource.clip = _Clips[0];
			_TrackDuration = _Clips[0].length / _ASourcePitch;
		}	

		private void Update()
		{
			MonitoringBPM();
			_TrackDuration -= Time.deltaTime;
			if (_TrackDuration <= _TrackOffsetLoad)
			{
				ChangeSong();
				_TrackDuration = _ASource.clip.length / _ASourcePitch;	
			}
		}
	#endregion UnityFunctions

	#region ClassFunctions
		private void ChangeSong()
		{
			AudioClip clp = null;
			switch (_LoopType)
			{
				case ELoopType.Random:
					clp = ChangeRandom();
					break;
				case ELoopType.RoundRobim:
					clp = ChangeRoundRobin(_ClipIndex);
					break;
				case ELoopType.InOrder:
					clp = ChangeInOrder();
					break;
				default:
					break;
			}
			_ASource.PlayOneShot(clp);
			_ASourcePitch = _ASource.pitch;
		}

		private void MonitoringBPM() 
		{
			_ASourcePitch = _ASource.pitch;
			if (_ASourcePitch != _ASourcePitchOld && _TrackDuration > _TrackOffsetLoad) 
			{
				_TrackDuration /= _ASourcePitch;
			}
			_ASourcePitchOld = _ASourcePitch;
		}

		private AudioClip ChangeRandom()
		{
			int index = Random.Range(0, _Clips.Count);
			AudioClip clip = _Clips[index];
			return clip;
		}

		private AudioClip ChangeRoundRobin(int clipIndex)
		{
			int index = (clipIndex + 1);
			// int index = (clipIndex + 1) % _Clips.Count;
			AudioClip clip = _Clips[index];
			return clip;
		}

		private AudioClip ChangeInOrder()
		{
			if(_ClipIndex >= _Clips.Count)
			{
				_ClipIndex = 0;
			}
			AudioClip clip = _Clips[_ClipIndex];
			_ClipIndex++;
			return clip;
		}

		#endregion ClassFunctions
}
