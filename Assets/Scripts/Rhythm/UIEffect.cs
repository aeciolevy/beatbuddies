﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEffect : MonoBehaviour {

	// private RectTransform _RT;
	private Image _Image;
	private Vector2 _StartSize;
	private void Awake()
	{
		// _RT = GetComponent<RectTransform>();
		_Image = GetComponent<Image>();
		_StartSize = _Image.rectTransform.sizeDelta;
	}

    public void OnbeatDetected()
    {
		_Image.rectTransform.sizeDelta = new Vector2(120, 120);
		StartCoroutine(DeathCoroutine());
    }

    IEnumerator DeathCoroutine()
    {
        yield return new WaitForSeconds(0.2f);
		resizeOriginal();
    }

	private void resizeOriginal(){
        _Image.rectTransform.sizeDelta = _StartSize;
	}
}
