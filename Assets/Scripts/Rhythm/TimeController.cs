﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

struct SpeedUp
{
	float timeToSpeedUp;
	float percentageToSpeedUp;
}

public class TimeController : MonoBehaviour 
{
	#region GlobalVars

		[Header("SpeedUp Settings")]
		[Tooltip("Time to Change the music BPM")]
		[SerializeField]
		private List<float> _TimeToSpeedUp;
		[SerializeField]
		[Range(0, 100)]
		private List<float> _PercentageToSpeedUp;

		private int _SpeedUpIndex = 0;
		private float _BeatFrequency = 1f;
		private float _BeatTime;
		private AudioSource[] _AudioSourceChildren;
		private float _MusicSpeed = 1f;
		private bool isToChangeMusicSpeed = false;

		[Header("Events")]
		public OnBeatEventHandler OnBeat;
		public OnSpectrumEventHandler onSpectrum;

	#endregion GlobalVars

	#region UnityFunctions
		private void Awake()
		{
			_AudioSourceChildren = GetComponentsInChildren<AudioSource>();
			_MusicSpeed = _AudioSourceChildren[0].pitch;
			_BeatFrequency = 1 / _MusicSpeed;
			_BeatTime = _BeatFrequency;
		}

		private void Update()
		{
			_BeatTime -= Time.deltaTime;
			if (_BeatTime <= 0)
			{
				_BeatTime = _BeatFrequency;
				OnBeat.Invoke();
			}
			if (!isToChangeMusicSpeed)
			{
				if (_TimeToSpeedUp.Count > 0)
				{
					_TimeToSpeedUp[_SpeedUpIndex] -= Time.deltaTime;
					if (_TimeToSpeedUp[_SpeedUpIndex] <= 0)
					{
						ChangeMusicSpeed();
					}
				}

			}
		}
		
    #endregion UnityFunctions

	#region Events
		
		[System.Serializable]
		public class OnBeatEventHandler : UnityEngine.Events.UnityEvent { }

		[System.Serializable]
		public class OnSpectrumEventHandler : UnityEngine.Events.UnityEvent<float[]> { }

    #endregion Events

    #region ClassFunctions
		private void ChangeMusicSpeed()
		{
        	_MusicSpeed = 1 + _PercentageToSpeedUp[_SpeedUpIndex] / 100;
			_BeatFrequency = 1 / _MusicSpeed;
			_BeatTime = _BeatFrequency;
        	foreach (AudioSource audio in _AudioSourceChildren)
			{
				audio.pitch = _MusicSpeed;
			}
			if (_SpeedUpIndex < _TimeToSpeedUp.Count - 1)
			{
				++_SpeedUpIndex;
			}
			else 
			{
				isToChangeMusicSpeed = true;
			}

		}
	#endregion ClassFunctions
}
